var canvas;
var gl = null,
	program = null;


function main(){

	canvas=document.getElementById("my-canvas");
	try{
		gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
	}catch(e){
		 console.log(e);
	}
	if(gl){
		
		//Setting the size for the canvas equal to half the browser window
		var w=canvas.clientWidth;
		var h=canvas.clientHeight;
		
		gl.clearColor(0.0, 0.0, 0.0, 1.0);
		gl.viewport(0.0, 0.0, w, h);
		
		var VBO = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, VBO);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
		
		//*** The (for now) obscure GLSL part... to take for granted.
			var vs = 'attribute vec3 pos1; void main() { gl_Position = vec4(pos1, 1); gl_PointSize = 5.0;}';
			var fs = 'precision mediump float; void main() { gl_FragColor = vec4(0.8,0,0,1); }';
			program = gl.createProgram();
			var v1 = gl.createShader(gl.VERTEX_SHADER);
			gl.shaderSource(v1, vs);
			gl.compileShader(v1);
			var v2 = gl.createShader(gl.FRAGMENT_SHADER);
			gl.shaderSource(v2, fs)
			gl.compileShader(v2);		
			gl.attachShader(program, v1);
			gl.attachShader(program, v2);
			gl.linkProgram(program);		
		//*** End of the obscure part
		
		gl.useProgram(program);
		
		var vertexPositionHandle = gl.getAttribLocation(program, 'pos1');
		gl.enableVertexAttribArray(vertexPositionHandle);
		gl.vertexAttribPointer(vertexPositionHandle, 3, gl.FLOAT, false, 3 * 4, 0);
		
		gl.drawArrays(gl.LINES, 0, 24);
		
	}else{
		alert( "Error: Your browser does not appear to support WebGL.");
	}
	
}




