import numpy as np
import math
import matplotlib
from sklearn import datasets
from sklearn import model_selection
from sklearn import linear_model
import sklearn.datasets.samples_generator
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import PolynomialFeatures

# Libraries for the evaluation
from sklearn import model_selection
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score

import matplotlib.pyplot as plt

def GenerateROCProblem(n_samples=10, random_state=1234):
    
    ### Color maps for plotting
    data_points_cm = matplotlib.colors.ListedColormap([plt.cm.Paired.colors[1],plt.cm.Paired.colors[3]], name='DataPointsColorMap')
    ds_points_cm = matplotlib.colors.ListedColormap([plt.cm.Paired.colors[0],plt.cm.Paired.colors[2]], name='DecisionSurfaceColorMap')

    ### 
    mx,my = datasets.samples_generator.make_moons(n_samples=n_samples, noise=0.3, random_state=1234)
#     plt.figure(1, figsize=(8, 6))
#     font = {'family':'sans', 'size':24}
#     plt.rc('font', **font)
#     plt.xlim([-3,3])
#     plt.ylim([-2,2])
#     #plt.scatter(mx[:,0], mx[:,1], c=train_y, cmap=data_points_cm, s=50);
#     plt.scatter(mx[:,0], mx[:,1], c=my, cmap=data_points_cm, s=50);
    logistic = linear_model.LogisticRegression(C=10e10)
    logistic.fit(mx,my)

    py = logistic.predict_proba(mx)

    import pandas as pd
    data = pd.DataFrame(mx)
    data['y'] = my
    data['P(y=1)'] = py[:,1]
    data.columns = ['x0','x1','y','P(y=1)']
    #data = data.sort(['P(y=1)'], ascending=[False])
    data = data.sort_values(by=['P(y=1)'], ascending=[False])
    data = data.reset_index(drop=True)
    n1 = int(.25*n_samples)
    n2 = int(.50*n_samples)
    n3 = int(.75*n_samples)

    print('Problem 1: Given the data below, where x0 and x1 are the variables, y is the')
    print('class attribute, and P(y=1) is the probability associated by a logistic')
    print('regression model, compute (1) accuracy (2) precision (3) recall and (4) F1')
    print('for the following probability thresholds:')
    print("- t1 = %.3f "%((data['P(y=1)'][n1]+data['P(y=1)'][n1+1])/2.0))
    print("- t2 = %.3f "%((data['P(y=1)'][n2]+data['P(y=1)'][n2+1])/2.0))
    print("- t3 = %.3f "%((data['P(y=1)'][n3]+data['P(y=1)'][n3+1])/2.0))
    print("\n\n")
    
    print(data)
    
    print('\n\n')
    print('Problem 2: Given the same data, where x0 and x1 are the variables, y is the')
    print('class attribute, and P(y=1) is the probability associated by a logistic')
    print('regression model, compute the ROC curve.\n\n')

    print('\n\n')
    print('Problem 3: Given the same data, where x0 and x1 are the variables, y is the')
    print('class attribute, and P(y=1) is the probability associated by a logistic')
    print('regression model, compute the Precision-Recall curve.\n\n')


    fpr, tpr, thresholds = roc_curve(y_true=my, y_score = py[:,1], pos_label=1)
    roc_auc = roc_auc_score(y_true=my, y_score = py[:,1])

    
    plt.figure(2, figsize=(8, 8));
    plt.title("Solution (ROC)")
    font = {'family':'sans', 'size':24};
    plt.rc('font', **font);
    plt.xlabel('FPR');
    plt.ylabel('TPR');
    plt.plot(fpr,tpr,label='AUC (%3.2f)'%roc_auc)
    plt.yticks(np.arange(0.0,1.01,.2))
    plt.legend()
    plt.show();
    
    precision, recall, thresholds = precision_recall_curve(y_true=my, probas_pred=py[:,1])
    auc = average_precision_score(my, py[:,1])
    
    plt.figure(3, figsize=(8, 8));
    plt.title("Solution (Precision-Recall)")
    font = {'family':'sans', 'size':24};
    plt.rc('font', **font);
    plt.xlabel('Recall');
    plt.ylabel('Precision');
    plt.plot(recall,precision,label='AUC (%3.2f)'%auc)
    plt.yticks(np.arange(0.0,1.01,.2))
    plt.legend()
    plt.show();
    