clear all
close all

%%%%%%%%%%%%
netname='wtn2008_scc1';
%%%%%%%%%%%%

load(strcat('A_',netname,'.mat'));

A=full(A);
s_in=sum(A); %row vector of node in-strength
s_out=sum(A')'; %column vector of node out-strength
s=s_in+s_out'; %row vector of node total strength
w_tot=sum(s_in); %total weight in the network
N=length(A); %number of nodes

disp([' '])
disp(['Network: ',netname,' - N = ',int2str(N)])

Abin=double(A>0);
k_in=sum(Abin); %row vector of node in-degrees
k_out=sum(Abin')'; %column vector of node out-degrees
k=k_in+k_out'; %row vector of node total degree
m=sum(k_in); %total number of links in the network

% wlink contains all the link weights
wlink=[squareform(A) squareform(A')]; 
wlink=wlink(find(wlink>0)); %vector of link weights
ntot=length(wlink);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%plotting the cumulative distribution of link weights
figure%('Position',[800,650,500,300])
f1=get(0,'CurrentFigure');
figure(f1)
nbins=30;
[n,xout]=hist(log(wlink),nbins);
n_norm=n/sum(n);
ncum=zeros(1,nbins);
for i=1:nbins
    ncum(i)=sum(n_norm(i:nbins));
end;
loglog(exp(xout),ncum)
xlabel('weight \it{w}')
title('cumulative link weight distribution')
grid on
drawnow

%plotting the cumulative distribution of in- and out- strengths
figure%('Position',[800,650,500,300])
f2=get(0,'CurrentFigure');
figure(f2)
nbins=15;

[n,xout]=hist(log(s_in),nbins);
n_norm=n/sum(n);
ncum=zeros(1,nbins);
for i=1:nbins
    ncum(i)=sum(n_norm(i:nbins));
end;
loglog(exp(xout),ncum)
drawnow
hold on

[n,xout]=hist(log(s_out),nbins);
n_norm=n/sum(n);
ncum=zeros(1,nbins);
for i=1:nbins
    ncum(i)=sum(n_norm(i:nbins));
end;
loglog(exp(xout),ncum,'r')
drawnow

[n,xout]=hist(log(s_in+s_out'),nbins);
n_norm=n/sum(n);
ncum=zeros(1,nbins);
for i=1:nbins
    ncum(i)=sum(n_norm(i:nbins));
end;
loglog(exp(xout),ncum,'m')
drawnow
hold on

xlabel('stengths \it{s_{in}} (blue),\it{s_{out}} (red),\it{s_{tot}} (magenta)')
title('cumulative in-, out-, and total strength distribution')
grid on


%plotting the cumulative distribution of in- and out- degrees
figure%('Position',[800,650,500,300])
f3=get(0,'CurrentFigure');
figure(f3)
nbins=15;

[n,xout]=hist(log(k_in),nbins);
n_norm=n/sum(n);
ncum=zeros(1,nbins);
for i=1:nbins
    ncum(i)=sum(n_norm(i:nbins));
end;
loglog(exp(xout),ncum)
drawnow
hold on

[n,xout]=hist(log(k_out),nbins);
n_norm=n/sum(n);
ncum=zeros(1,nbins);
for i=1:nbins
    ncum(i)=sum(n_norm(i:nbins));
end;
loglog(exp(xout),ncum,'r')
drawnow

[n,xout]=hist(log(k_in+k_out'),nbins);
n_norm=n/sum(n);
ncum=zeros(1,nbins);
for i=1:nbins
    ncum(i)=sum(n_norm(i:nbins));
end;
loglog(exp(xout),ncum,'m')
drawnow
hold on

xlabel('degrees \it{k_{in}} (blue),\it{k_{out}} (red),\it{k_{tot}} (magenta)')
title('cumulative in-, out-, and total degree distribution')
grid on



