
#include <stdio.h>
#include <pthread.h>
#include <list>

using namespace std;

pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond=PTHREAD_COND_INITIALIZER; 
list<int> data;

void *threadFunction(void *arg)
{
    for(;;)
    {
        pthread_mutex_lock(&mutex);
        while(data.empty()) pthread_cond_wait(&cond,&mutex);
        printf("Received %d\n",data.front());
        data.pop_front();
        pthread_mutex_unlock(&mutex);
    }
}

int main()
{
    pthread_t t;
    pthread_create(&t,NULL,&threadFunction,NULL);
    for(;;)
    {
        int i;
        scanf("%d",&i);
        pthread_mutex_lock(&mutex);
        data.push_back(i);
        pthread_cond_broadcast(&cond);
        pthread_mutex_unlock(&mutex);
    }
}
